package array;

import java.util.ArrayList;
import java.util.Iterator;

public class Arrayloop {

	public static void main(String[] args) {
		ArrayList<Number> arraylist = new ArrayList<Number>();
		arraylist.add(17);
		arraylist.add(14);
		arraylist.add(24);
		arraylist.add(2);
		
		System.out.println("Looping array list using Iterator\n");
		Iterator itr=arraylist.iterator();
		while(itr.hasNext()) {
			System.out.println(itr.next());
		}
		
		System.out.println("Looping array list using For loop\n");
		for(int number=0;number<arraylist.size();number++) {
			System.out.println(arraylist.get(number));
		}
		
		System.out.println("Looping array list using Advance For loop\n");
		for(Number n1:arraylist) {
			System.out.println(n1);
		}	
		System.out.println("Looping array list using While loop\n");
		int num=0;
		while(arraylist.size()>num) {
			System.out.println(arraylist.get(num));
			num++;
		}
			
			
		

	}

}
