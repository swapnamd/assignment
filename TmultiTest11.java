package Annotations;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TmultiTest11 {

	@BeforeAll
	static void setUpBeforeClass()  {
		TMultiply dv=new TMultiply();
		assertEquals(5.0,dv.div(10, 2));
		System.out.println("excuete all");
	}

	@AfterAll
	static void tearDownAfterClass()  {
		
		TMultiply sb=new TMultiply();
		assertEquals(18,sb.sub(50, 30, 2));
		System.out.println("excuete");
	}

	@BeforeEach
	void setUp() {
		TMultiply tm=new TMultiply();
		assertEquals(3,tm.avg(4, 3, 2));
		System.out.println(" all");
	}

	

	@AfterEach
	void tearDown()  {
		
		TMultiply ad=new TMultiply();
		assertEquals(11,ad.add1(2, 1, 3, 5));
		System.out.println("each line");
	}

	@Test
	void test() {
		TMultiply tp=new TMultiply();
		assertEquals(81,tp.Multi(9,9));
	}

}
