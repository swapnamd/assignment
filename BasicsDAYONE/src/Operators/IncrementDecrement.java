package Operators;

public class IncrementDecrement {

	public static void main(String[] args) {
		// ++ -- 
		int n1=20;
		int n2=10;
		System.out.println(n1);//n1 value is 20
		System.out.println(++n1);//n1 value is 21
		//System.out.println(n1++);//n1 value is 21
		System.out.println(--n1);
		//System.out.println(n1--);
		System.out.println(++n2);
		//System.out.println(n2++);
		//System.out.println(n2--);
		System.out.println(--n2);

	}

}
