package ArrayString;

public class NamesAge 
{

	public static void main(String[] args) 
	{
		//create a array
		int age[]={14,17,24,33,48};
		//System.out.println("Print all the age in a array");
		System.out.println("Age in second place is:");
		System.out.println((age[4]));
		System.out.println(age.length);
		System.out.println(age);
		//System.out.println(age.clone());
		
		
		String friendsname[] = new String[5];
		friendsname[0]="Priya";
		friendsname[1]="Thanu";
		friendsname[2]="Sneha";
		friendsname[3]="Harshitha";
		friendsname[4]="Ritika";
		System.out.println(friendsname[1]);
		System.out.println(friendsname.length);
	}

}
