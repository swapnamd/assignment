package MethodObject;

public class First
{
	//create a method
	public void birds()
	{
		System.out.println("I like parrot");
	}

	public static void main(String[] args) 
	{
		System.out.println("Parrot color is green ");
		//create a object to call method
		First f = new First();
		f.birds();
	}

}
