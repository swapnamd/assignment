package ConditionalStmt;
import java.util.Scanner;

public class ScannerMarksIfElse 
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Marks");
		int marks=sc.nextInt();
		System.out.println("Enter the English marks");
		int english=sc.nextInt();
		System.out.println("Enter the Maths");
		int maths=sc.nextInt();
		System.out.println("Enter the Science");
		int science=sc.nextInt();
		int total=english+maths+science;
		System.out.println("Total marks is:"+total);
		
		int avg=total/3;
		System.out.println("Average mark is:"+avg);
		
		if(avg<=35) 
		{
			System.out.println("He/She is just pass with c+ grade");
			
		}
		else if(avg<=35&&avg>=65) {
			System.out.println("He/She is pass with A++,A,B++,B grade");
		}
		else if(avg<=66&&avg>=85) {
			System.out.println("He/She is first class with good marks");
		}
		else {
			System.out.println("He/She is distinstion");
		}
	}

}
