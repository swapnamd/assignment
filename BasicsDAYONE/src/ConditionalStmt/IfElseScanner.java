package ConditionalStmt;
import java.util.Scanner;

public class IfElseScanner 
{

	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the age:");
		int age=sc.nextInt();
		if(age<18) {
			System.out.println("Not eligible to vote");
		}
		else if(age>100) {
			System.out.println("Please stop voting");
		}
		else
		{
			System.out.println("Eligible to vote");
		}
	}

}
