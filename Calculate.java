package maxAndmin;
import java.util.Scanner;
import java.util.*;

public class Calculate 
{
	
	public static void main(String[] args) 
	{
		System.out.println("Enter the array numbers:");
		Scanner sc=new Scanner(System.in);
		int arr[] = new int[5];
		int sum=0;
		
		for(int i=0;i<5;i++) {
			arr[i]=sc.nextInt();
			sum+=arr[i];
			
		}
		Arrays.sort(arr);
		int min=arr[4];
		int max=arr[0];
		int minSum=sum-min;
		int maxSum=sum-max;
		
		System.out.println("minimum array sum is:"+minSum);
		System.out.println("maximum array sum is:"+maxSum);

}
}
