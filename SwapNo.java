package swapping;

import java.util.Scanner;

public class SwapNo {

	public static void main(String[] args) 
	{
		int x,y,temp ;
		Scanner sc =new Scanner(System.in);
		System.out.println("Enter the value of x:");
		x=sc.nextInt();
		System.out.println("Enter the value of y:");
		y=sc.nextInt();
		temp=x;
		x=y;
		y=temp;
		System.out.println("After swappin x value is:"+x+"\nAfter swapping y value is:"+y);
		//System.out.println();
	}

}
