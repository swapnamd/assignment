package Time;

//import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class TimeFormat {
//
	public static void main(String[] args) throws ParseException {
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the 12 hours format time....:");
		String s=sc.nextLine();
		SimpleDateFormat displayFormat=new SimpleDateFormat("hh:mm:ssaa");
		SimpleDateFormat changeFormat=new SimpleDateFormat("HH:mm:ss");
		
		Date date=null;
		date=displayFormat.parse(s);
		if(date!=null) {
			String myDate=changeFormat.format(date);
			System.out.println(myDate);
		}
		sc.close();
		

	}

}
